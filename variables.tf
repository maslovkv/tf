###cloud vars
variable "service_account_key_file" {
  type        = string
  description = "OAuth-token; https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-token"
}

variable "cloud_id" {
  type        = string
  description = "https://cloud.yandex.ru/docs/resource-manager/operations/cloud/get-id"
}

variable "folder_id" {
  type        = string
  description = "https://cloud.yandex.ru/docs/resource-manager/operations/folder/get-id"
}

variable "default_zone" {
  type        = string
  default     = "ru-central1-a"
  description = "https://cloud.yandex.ru/docs/overview/concepts/geo-scope"
}
variable "default_cidr" {
  type        = list(string)
  default     = ["192.168.10.0/24"]
  description = "https://cloud.yandex.ru/docs/vpc/operations/subnet-create"
}
variable "private_cidr" {
  type        = list(string)
  default     = ["192.168.20.0/24"]
  description = "https://cloud.yandex.ru/docs/vpc/operations/subnet-create"
}

variable "vpc_name" {
  type        = string
  default     = "netology"
  description = "VPC network&subnet name"
}
variable "subnet_public" {
  type        = string
  default     = "public"
  description = "VPC network&subnet name"
}
variable "subnet_private" {
  type        = string
  default     = "private"
  description = "VPC network&subnet name"
}
variable "public_key" {
  type    = string
  default = ""
}
variable "docker_login" {
  type    = string
  default = ""
}
variable "docker_key" {
  type    = string
  default = ""
}
variable "username" {
  type = string
  default = ""
}
variable "tf_r_key" {
  type = string
  default = ""
}
variable "web_r_key" {
  type = string
  default = ""
}
variable "ssh_public_key" {
  type        = string
  description = "Location of SSH public key."
}

variable "packages" {
  type    = list(any)
  default = []
}

variable "vm_resources" {
  default = {
    cores         = 2
    memory        = 2
    core_fraction = 20
    boot_disk_mode = "READ_WRITE"
    platform_id = "standard-v3"
    disk_size = 30
    disk_type = "network-hdd"
  }
}
variable "vm_image_family" {
  type        = string
  description = "OS release name"
  default     = "ubuntu-2004-lts"
}
data "yandex_compute_image" "ubuntu" {
  family = var.vm_image_family
}
data "template_file" "cloudinit" {
  template = file("./cloud-init.yml")
  vars = {
    username       = var.username
    ssh_public_key = file(var.ssh_public_key)
    packages       = jsonencode(var.packages)
  }
}

variable "sa_name" {
  type = string
  default = ""
}

variable "k8s-net" {
  type    = string
  default = "k8s-net"
}

variable  "k8s-subnet-a" {
  type = map(any)
  default = {
    name           = "k8s-subnet-a"
    zone           = "ru-central1-a"
    v4_cidr_blocks = "10.5.0.0/24"
  }
}

variable  "k8s-subnet-b" {
  type = map(any)
  default = {
    name           = "k8s-subnet-b"
    zone           = "ru-central1-b"
    v4_cidr_blocks = "10.6.0.0/24"
  }
}

variable  "k8s-subnet-d" {
  type = map(any)
  default = {
    name           = "k8s-subnet-d"
    zone           = "ru-central1-d"
    v4_cidr_blocks = "10.7.0.0/24"
  }
}

variable "k8s_node_group" {
  default = {
    region      = "ru-central1"
    name        = "k8s-node-group"
    version     = "1.28"
    min         = 3
    max         = 6
    initial     = 3
    container_runtime = "containerd"
 }
}


variable "bucket" {
  type = map(any)
  default = {
    name = "mkv-terraform-diplom"
    max_size  = "1073741824"
    storage_class = "STANDARD"
    key = "terraform.tfstate"
    dynamodb_endpoint = "https://docapi.serverless.yandexcloud.net/ru-central1/b1g8isf6e7r1t01j6ejk/etnh7au2qt7ncms7727b"
    dynamodb_table = "tfstate-diplom-netology"


  }

}
