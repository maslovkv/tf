resource "yandex_compute_instance" "cp" {
  count = 1
  name        = "cp-${count.index + 1}"
  hostname    = "cp-${count.index + 1}"
  platform_id = var.vm_resources.platform_id

  resources {
    cores         = var.vm_resources.cores
    memory        = var.vm_resources.memory
    core_fraction = var.vm_resources.core_fraction
  }

  boot_disk {
    initialize_params {
      size = var.vm_resources.disk_size
      image_id = data.yandex_compute_image.ubuntu.image_id
    }
  }
  scheduling_policy {
    preemptible = true
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.k8s-subnet-a.id
    nat       = true
    security_group_ids = ["${yandex_vpc_security_group.regional-k8s-sg.id}", "${yandex_vpc_security_group.k8s-master-whitelist.id}"]
  }

  metadata = {
    user-data = data.template_file.cloudinit.rendered
  }
  allow_stopping_for_update = true

}

resource "yandex_compute_instance" "node" {
  count = 2
  name        = "node-${count.index + 1}"
  hostname    = "node-${count.index + 1}"
  platform_id = var.vm_resources.platform_id

  resources {
    cores         = var.vm_resources.cores
    memory        = var.vm_resources.memory
    core_fraction = var.vm_resources.core_fraction
  }

  boot_disk {
    initialize_params {
      size = var.vm_resources.disk_size
      image_id = data.yandex_compute_image.ubuntu.image_id
    }
  }
  scheduling_policy {
    preemptible = true
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.k8s-subnet-a.id
    nat       = true
    security_group_ids = ["${yandex_vpc_security_group.regional-k8s-sg.id}", "${yandex_vpc_security_group.k8s-master-whitelist.id}"]
  }

  metadata = {
    user-data = data.template_file.cloudinit.rendered
  }
  allow_stopping_for_update = true

}

resource "yandex_compute_instance" "run" {
  count = 2
  name        = "run-${count.index + 1}"
  hostname    = "run-${count.index + 1}"
  platform_id = var.vm_resources.platform_id

  resources {
    cores         = var.vm_resources.cores
    memory        = var.vm_resources.memory
    core_fraction = var.vm_resources.core_fraction
  }

  boot_disk {
    initialize_params {
      size = var.vm_resources.disk_size
      image_id = data.yandex_compute_image.ubuntu.image_id
    }
  }
  scheduling_policy {
    preemptible = true
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.k8s-subnet-a.id
    nat       = true
    security_group_ids = ["${yandex_vpc_security_group.regional-k8s-sg.id}", "${yandex_vpc_security_group.k8s-master-whitelist.id}"]
  }

  metadata = {
    user-data = data.template_file.cloudinit.rendered
  }
  allow_stopping_for_update = true

}


