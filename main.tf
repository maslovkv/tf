resource "yandex_iam_service_account" "my-regional-account" {
  name        = "regional-k8s-account"
  description = "K8S regional service account"
}

resource "yandex_resourcemanager_folder_iam_member" "vpc-public-admin" {
  # Сервисному аккаунту назначается роль "vpc.publicAdmin".
  folder_id = var.folder_id
  role      = "vpc.publicAdmin"
  member    = "serviceAccount:${yandex_iam_service_account.my-regional-account.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "images-puller" {
  # Сервисному аккаунту назначается роль "container-registry.images.puller".
  folder_id = var.folder_id
  role      = "container-registry.images.puller"
  member    = "serviceAccount:${yandex_iam_service_account.my-regional-account.id}"
}
resource "local_file" "sa-static-key" {
    content  = "B_A_KEY=\"${yandex_iam_service_account_static_access_key.sa-static-key.access_key}\"\nB_S_KEY=\"${yandex_iam_service_account_static_access_key.sa-static-key.secret_key}\""
    filename = "sa-static-key"
    directory_permission = "0600"
    file_permission      = "0600"

}


resource "local_file" "hosts_cfg" {
  content = templatefile("${path.module}/hosts.tftpl",

    { manager =  yandex_compute_instance.cp,
      nodes = yandex_compute_instance.node,
      runner =  yandex_compute_instance.run,
      username = data.template_file.cloudinit.vars.username
    }

  )
  filename = "${abspath(path.module)}/hosts.cfg"
}

resource "null_resource" "k8s_hosts_provision" {

#Ждем создания нод
depends_on = [yandex_compute_instance.cp,yandex_compute_instance.node,local_file.hosts_cfg,local_file.sa-static-key]

#Костыль!!! Даем ВМ время на первый запуск. Лучше выполнить это через wait_for port 22 на стороне ansible
 provisioner "local-exec" {
    command = "sleep 60"
  }

#Запуск ansible-playbook
  provisioner "local-exec" {
    command  = "export ANSIBLE_HOST_KEY_CHECKING=False; ansible-playbook -i ${abspath(path.module)}/hosts.cfg ${abspath(path.module)}/k8s.yml  --extra-vars \"api_a_a=${yandex_compute_instance.cp[0].network_interface.0.ip_address} api_ces=${yandex_compute_instance.cp[0].network_interface.0.nat_ip_address} cp_name=${yandex_compute_instance.cp[0].hostname} \""

    on_failure = continue #Продолжить выполнение terraform pipeline в случае ошибок
    environment = { ANSIBLE_HOST_KEY_CHECKING = "False" }
    #срабатывание триггера при изменении переменных
  }
    triggers = {
      always_run         = "${timestamp()}" #всегда т.к. дата и время постоянно изменяются
      playbook_src_hash  = file("${abspath(path.module)}/k8s.yml") # при изменении содержимого playbook файла
      ssh_public_key     = var.public_key # при изменении переменной
      ip_k8s = "${yandex_compute_instance.cp[0].network_interface.0.nat_ip_address}"
    }

}

