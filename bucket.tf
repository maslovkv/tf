
// Назначение роли сервисному аккаунту
resource "yandex_resourcemanager_folder_iam_member" "sa-editor" {
  folder_id = var.folder_id
  role      = "storage.editor"
  member    = "serviceAccount:${yandex_iam_service_account.my-regional-account.id}"
}
// Назначение роли сервисному аккаунту
resource "yandex_resourcemanager_folder_iam_member" "editor" {
  folder_id = var.folder_id
  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.my-regional-account.id}"
}

// Создание статического ключа доступа
resource "yandex_iam_service_account_static_access_key" "sa-static-key" {
  service_account_id = yandex_iam_service_account.my-regional-account.id
  description        = "static access key for object storage"
}

// Создание симметричного ключа
#resource "yandex_kms_symmetric_key" "key-a" {
#  name              = "example-symetric-key"
#  description       = "description for key"
#  default_algorithm = "AES_128"
#  rotation_period   = "8760h" // equal to 1 year
#}



// Создание бакета с использованием ключа
resource "yandex_storage_bucket" "bucket" {
#  depends_on = [yandex_kms_symmetric_key.key-a]
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket     = var.bucket.name
  max_size   = var.bucket.max_size
  default_storage_class = var.bucket.storage_class
  force_destroy = true
// Шифорвание содержимого бакета
#  server_side_encryption_configuration {
#    rule {
#      apply_server_side_encryption_by_default {
#        kms_master_key_id = yandex_kms_symmetric_key.key-a.id
#        sse_algorithm     = "aws:kms"
#      }
#    }
#  }
}

