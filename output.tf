output "control_plane_public_ip" {
  description = "Public IP addresses for control-plane"
  value       = yandex_compute_instance.cp[0].network_interface.0.nat_ip_address
}
output "nodes_public_ip" {
  description = "Public IP addresses for control-plane"
  value       = [yandex_compute_instance.node.*.network_interface.0.nat_ip_address]
}
output "runner_public_ip" {
  description = "Public IP addresses for control-plane"
  value       = [yandex_compute_instance.run.*.network_interface.0.nat_ip_address]
}
output "lb" {
  description = "Public IP addresses for control-plane"
  value       = yandex_vpc_address.web.external_ipv4_address[0].address
}