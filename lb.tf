resource "yandex_lb_target_group" "web" {
  name = "web-target-group"

  dynamic "target" {
    for_each = [for s in yandex_compute_instance.node : {
     subnet_id= s.network_interface.0.subnet_id
     address   = s.network_interface.0.ip_address
    }]

    content {
      subnet_id  = target.value.subnet_id
      address = target.value.address
    }
  }
}
resource "yandex_lb_network_load_balancer" "web" {
  depends_on = [yandex_lb_target_group.web]
  name = "web"

  listener {
    name = "web"
    port = 80
    external_address_spec {
      address = yandex_vpc_address.web.external_ipv4_address[0].address
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.web.id

    healthcheck {
      name = "tcp"
      tcp_options {
        port = 10256
      }
    }
  }
}

